// =============== IMPORT ===============

import gulp from 'gulp';
import babel from 'gulp-babel';
import sass from 'gulp-sass';
import uglify from 'gulp-uglify';
import concat from 'gulp-concat';
import autoprefixer from 'gulp-autoprefixer';
import clean from 'gulp-clean-css';
import browserSync from 'browser-sync';
import del from 'del';

// =============== VARIABLES ===============

const sync = browserSync.create();
const reload = sync.reload;
const config = {
  paths: {
    src: {
      html: './src/**/*.html',
      images: './src/images/**.*',
      style: ['src/style/app.scss'],
      js: [
        'src/js/all.js',
      ]
    },
    build: {
      main: './build',
      css: './build/css',
      js: './build/js',
      images: './build/images'
    }
  }
};

// =============== STYLE TASK ===============

gulp.task('style', () => {
  return gulp.src(config.paths.src.style)
    .pipe(sass())
    .pipe(autoprefixer({
        browsers: ['last 2 versions']
    }))
    .pipe(clean())
    .pipe(gulp.dest(config.paths.build.css))
    .pipe(sync.stream());
});

// =============== JS TASK ===============

gulp.task('js', () => {
  gulp.src(config.paths.src.js)
    .pipe(babel({ presets: ['env'] }))
    .pipe(concat('all.js'))
    .pipe(uglify())
    .pipe(gulp.dest(config.paths.build.js));

    reload();
});

// =============== STATIC TASK ===============

gulp.task('static', () => {
  gulp.src(config.paths.src.html)
    .pipe(gulp.dest(config.paths.build.main));

  gulp.src(config.paths.src.images)
    .pipe(gulp.dest(config.paths.build.images));

  reload();
});

// =============== CELAN TASK ===============

gulp.task('clean', () => {
  return del([config.paths.build.main]);
});

// =============== SERVER TASK ===============

gulp.task('server', () => {
  sync.init({
    injectChanges: true,
    server: config.paths.build.main
  });
});

// =============== WATCH TASK ===============

gulp.task('watch', ['default'], function () {
  gulp.watch('src/style/app.scss', ['style']);
  gulp.watch('src/js/**/*.js', ['js']);
  gulp.watch('src/*.html', ['static']);
  gulp.start('server');
});

// =============== BUILD TASK ===============

gulp.task('build', ['clean'], function () {
  gulp.start('style', 'js', 'static');
});

gulp.task('default', ['build']);